var searchData=
[
  ['filename_5fhas_5fpalz',['filename_has_PALZ',['../palz_8c.html#a281427a00a113ea9559f9dd6d634f21c',1,'filename_has_PALZ(const char *filename):&#160;palz.c'],['../palz_8h.html#a281427a00a113ea9559f9dd6d634f21c',1,'filename_has_PALZ(const char *filename):&#160;palz.c']]],
  ['folder_5fdecompress_5farg',['folder_decompress_arg',['../structgengetopt__args__info.html#a6f2e1a9f1df48c4cf172e31e55cb60ca',1,'gengetopt_args_info']]],
  ['folder_5fdecompress_5fgiven',['folder_decompress_given',['../structgengetopt__args__info.html#aec8278fee5127fd96d486ef7f77d03b8',1,'gengetopt_args_info']]],
  ['folder_5fdecompress_5fhelp',['folder_decompress_help',['../structgengetopt__args__info.html#a21046d178c3c579d6246dadefda3ae50',1,'gengetopt_args_info']]],
  ['folder_5fdecompress_5forig',['folder_decompress_orig',['../structgengetopt__args__info.html#ab0e2a84c99dfe0b7b1d2b1410926f1c7',1,'gengetopt_args_info']]],
  ['free',['FREE',['../memory_8h.html#a105949c59c998e38aad80266afac92bf',1,'memory.h']]],
  ['free_5fdictionary',['free_dictionary',['../palz_8c.html#a9e494210c588bc2c5c69ea9fd42a7eae',1,'free_dictionary(char **dictionary, const unsigned int dictionary_size):&#160;palz.c'],['../palz_8h.html#a9e494210c588bc2c5c69ea9fd42a7eae',1,'free_dictionary(char **dictionary, const unsigned int dictionary_size):&#160;palz.c']]]
];
