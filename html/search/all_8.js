var searchData=
[
  ['initialize',['initialize',['../structcmdline__parser__params.html#a97ed8a6eabd39291ae7d73f273e12c11',1,'cmdline_parser_params']]],
  ['install_5fsignal_5fhandler',['install_signal_handler',['../palz_8c.html#abc72b12c7959ec9a5b3aa5269e4f7063',1,'install_signal_handler(void):&#160;palz.c'],['../palz_8h.html#abc72b12c7959ec9a5b3aa5269e4f7063',1,'install_signal_handler(void):&#160;palz.c']]],
  ['is_5fheader_5fpalz',['is_header_PALZ',['../palz_8c.html#a3d5d2557afd9f3d721f3adafcfbbf68a',1,'is_header_PALZ(const char *header_first_row):&#160;palz.c'],['../palz_8h.html#a3d5d2557afd9f3d721f3adafcfbbf68a',1,'is_header_PALZ(const char *header_first_row):&#160;palz.c']]],
  ['is_5fvalid_5fsize',['is_valid_size',['../palz_8c.html#aa32fbfcfd9912c0a0ca496f811b44912',1,'is_valid_size(const char *size_str, unsigned int *value):&#160;palz.c'],['../palz_8h.html#aa32fbfcfd9912c0a0ca496f811b44912',1,'is_valid_size(const char *size_str, unsigned int *value):&#160;palz.c']]]
];
