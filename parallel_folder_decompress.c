#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <errno.h>
#include <dirent.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <pthread.h>

#include "parallel_folder_decompress.h"
#include "debug.h"
#include "listas.h"
#include "memory.h"
#include "palz.h"
#include "printdir.h"

#define C_ERRO_PTHREAD_CREATE   1
#define C_ERRO_PTHREAD_JOIN     2
#define C_ERRO_MUTEX_INIT       3
#define C_ERRO_MUTEX_DESTROY    4
#define C_ERRO_COND_INIT        5
#define C_ERRO_COND_DESTROY     6
#define ERR_THREAD				7
#define ERR_FREAD				9

#define MAX 5 /* Capacidade do buffer */
#define LIMITE 20 /* Total de elementos a produzir */

/**
* This function receives a directory and decompresses all files in this
* folder and its subfolders, using threads.
*
* @param *dir - pointer to the directory
* @param max_threads - maximum number of threads to use
*/

void parallel_folder_decompress(char *dir, int max_threads){

	PARAM_T param;

	/* Inicia o mutex */
	if ( (errno =  pthread_mutex_init(&param.mutex, NULL) ) != 0) {
		ERROR ( C_ERRO_MUTEX_INIT,   "pthread_mutex_init() failed! ");
	}
	
	/* Inicia variavel de condicao */
	if ( (errno =  pthread_cond_init(&param.cond, NULL) ) != 0) {
		ERROR ( C_ERRO_COND_INIT, "pthread_cond_init() failed! ");
	}

	/* Inicia os restantes parametros a passar 'as threads */
	param.buffer_size = 2*max_threads;
	param.buffer = MALLOC(sizeof(char *) * param.buffer_size);
	param.total = 0;
	param.index_escrita = 0;
	param.index_leitura = 0;
	param.stop=0; 

	/*alocar memoria para o vetor de ponteiro*/	
	pthread_t* consumers = MALLOC(sizeof(pthread_t) * max_threads);
	
	if(consumers == NULL){
		ERROR(ERR_THREAD, "MALLOC() failed!");
	}
	
	int i;
	
	for(i=0;i<max_threads;i++){
		if((errno = pthread_create(&consumers[i], NULL, consumer_decompress, &param)) != 0){
			ERROR(C_ERRO_PTHREAD_CREATE, "pthread_create() failed!");
		}
	}	

	producer_decompress(dir, &param);

	if((errno = pthread_mutex_lock(&param.mutex))!=0){
		ERROR(ERR_THREAD, "pthread_mutex_lock() failed!");
	}
	//when stop = 1 halts
	//when stop = 0 continues
	param.stop=1;
	
	if((errno = pthread_cond_broadcast(&param.cond))!=0){
		ERROR(ERR_THREAD, "pthread_cond_broadcast() failed!");
	}	

	if((errno = pthread_mutex_unlock(&param.mutex))!=0){
		ERROR(ERR_THREAD, "pthread_mutex_unlock() failed!");
	}
	
	for(i=0;i<max_threads;i++){
		if((errno = pthread_join(consumers[i], NULL)) != 0){
		ERROR(C_ERRO_PTHREAD_JOIN, "pthread_join() failed! ");
		}
	}
	
	FREE(param.buffer);
	FREE(consumers);

	/* Destroi a condic ao */
	if (   (errno =  pthread_cond_destroy(&param.cond) )   != 0) {
		ERROR ( C_ERRO_COND_DESTROY, " pthread_cond_destroy failed! ");
	}
	
	/* Destroi o mutex */
	if (   (errno =  pthread_mutex_destroy(&param.mutex) )   != 0) {
		ERROR ( C_ERRO_MUTEX_DESTROY, "pthread_mutex_destroy   failed! ");
	}
}

/**
* When created, a consumer thread runs this function. It will keep
* waiting until it has a task (to consume), proceding to the file
* decompression when given the task, and alerting the producers when it 
* is done.
*
*/

void *consumer_decompress(void *arg){
	PARAM_T *param = (PARAM_T *) arg; 

	while(1){
		if ((errno = pthread_mutex_lock(&(param -> mutex)))!= 0) {
			ERROR(ERR_THREAD, "pthread_mutex_lock() failed! ");
		}
		
		/* Espera que o buffer tenha dados */
		while (param -> total == 0 && !param->stop){
			if ( (errno=pthread_cond_wait(&(param -> cond),&(param -> mutex)))!=0 ) {
				ERROR(ERR_THREAD, "pthread_cond_wait() failed! ");
			}
		}
		
		if(param->total == 0){					
			if((errno = pthread_mutex_unlock(&param->mutex))!=0){
				ERROR(ERR_THREAD, "pthread_mutex_unlock() failed! ");
			}
			break;
		}
		
		/* Retira um valor no buffer */
		char *filename = param->buffer[param->index_leitura];
		param -> index_leitura = (param -> index_leitura + 1) % param->buffer_size;
		param -> total--;
		
		/* Notifica produtores 'a espera */
		if (param -> total == param->buffer_size - 1){
			if ((errno = pthread_cond_signal(&(param -> cond))) != 0) {
				ERROR(ERR_THREAD, "pthread_cond_signal() failed! ");
			}
		}
		/* Sai da seccao critica */
		if ((errno=pthread_mutex_unlock(&(param -> mutex))) != 0) {
			ERROR(ERR_THREAD, "pthread_mutex_unlock() failed! ");
		}
		decompress(filename);
		FREE(filename);
	}
	return NULL;
}

/**
* A producer thread will keep running this function until it runs out
* of folder or files to read or until the buffer is filled. When it 
* finds a file, it adds it to the buffer and alerts the consumers
* that there is a file ready to be "consumed".
*
* @param *dir - pointer to the directory being searched
* @param *param - pointer to the structure
*/

void producer_decompress(char *dir, PARAM_T *param){
	struct dirent entry, *result = NULL;
	struct stat stats;
	char *fullname=NULL, *folder=NULL;
	LISTA_GENERICA_T *list_ptr = lista_criar(free);

	lista_inserir(list_ptr, strdup(dir));

	while((folder = lista_remover_inicio(list_ptr)) != NULL){
		DIR * dir_ptr = opendir(folder);
		
		if(dir_ptr == NULL){
			ERROR(ERR_FILE, "opendir() failed!");
		}
		while(readdir_r(dir_ptr, &entry, &result)==0 && result != NULL ){ //&& running
			if(strcmp(entry.d_name,".")==0||strcmp(entry.d_name,"..")==0){
				continue;
			}
			
			build_fullname(folder, entry.d_name, &fullname);
			sprintf(fullname, "%s/%s", folder, entry.d_name);

			if(lstat(fullname, &stats)!=0){
				ERROR(ERR_FILE, "lstat failed on folder %s\n", fullname);
			}

			if(S_ISDIR(stats.st_mode)){
				lista_inserir(list_ptr, strdup(fullname));
			}
			else if(S_ISREG(stats.st_mode)){ 
			//verificar o ficheiro. tem extensao "palz" ou "PALZ"

				if ((errno = pthread_mutex_lock(&(param->mutex)))   != 0) {
					ERROR(ERR_FREAD, "pthread_mutex_lock() failed!");
				}
				
				/* Espera que o buffer tenha espaco disponivel */
				while (param ->total == param -> buffer_size ){ //&& running

					if ( (errno=pthread_cond_wait(&(param ->cond), &(param ->mutex)) )!=0) {
						ERROR(ERR_FREAD, "pthread_cond_wait() failed! ");
					}	
				}

				/* Coloca um valor no buffer */
				param->buffer[param->index_escrita]=strdup(fullname);
				param->index_escrita=(param->index_escrita+1)%param->buffer_size;
				param->total++;

				/* Notifica comsumidores 'a espera */
				if(param->total==1){
					if((errno=pthread_cond_broadcast(&(param->cond)))!=0){
						ERROR(ERR_THREAD, "pthread_cond_broadcast() failed!");
					}
				}
				/* Sai da seccao */ 
				if((errno=pthread_mutex_unlock(&(param->mutex)))!=0){
						ERROR(ERR_THREAD, "pthread_mutex_unlock() failed!");
				}
			}	
		}
		
		closedir(dir_ptr);
		FREE(folder);
	}
	lista_destruir(&list_ptr);
	FREE(fullname);	
}
