/**
* @file palz.h
* @brief This file has the declarations of the functions in palz.c and the
* defines used in that file.
* @author Beatriz Franco, Eduardo Andrade
*/

#ifndef PALZ_H
#define PALZ_H

#include "hashtables.h"

#define MAX_LINE 1024
#define ERR_FILE 2
#define PALZ "PALZ\n"
#define DELIMITERS "\n\t\r ?!.;,:+-*/"

/*Declaração de funções */

void decompress(const char *filename);
char ** read_header(const char *filename, unsigned int *dictionary_size, FILE *fptr);
void free_dictionary(char **dictionary, const unsigned int dictionary_size);
int is_header_PALZ(const char *header_first_row);
int is_valid_size(const char *size_str, unsigned int *value);
void decompress_file(const char *filename, char **dictionary, unsigned int *dictionary_size, FILE *fptr);
char *filename_has_PALZ(const char *filename);
char *string_to_upper(char *string);
unsigned int bytes_for_int(unsigned int value);
void install_signal_handler(void);
void trata_sinal(int signal);
void compress(char *filename);
HASHTABLE_T* find_words(FILE *fptr, char *delimiters);
void list_to_array(LISTA_GENERICA_T *list, char **dictionary_array);
int cmpstring(const void *p1 , const void *p2);
void compress_file(FILE *fptr, FILE *fptr_new, HASHTABLE_T hashtable, int num_list_elements);
int get_word_value(HASHTABLE_T hashtable, char *word);
void handler(int signal);
int get_limit_value(int bytes);
void build_word(int *lastDelim, char *word, int useLastDelim, char c1[1000], char c2, int *wordFound);
int verify_valid_file(char *filename);
void print_file_compression(char *filename, FILE *fptr, FILE *fptr_new);	

#endif
