/**
* @file palz.c
* @brief This file has the functions used to read and create the dictionary
* of a PALZ file, to decompress it and write the new decompressed file.
* It also has various checks and verifications to ensure the file is not
* corrupted or impossible to decompress.
* @author Beatriz Franco, Eduardo Andrade
*/

#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <unistd.h>
#include <signal.h>
#include <errno.h>
#include <string.h>
#include <sys/types.h>
#include <sys/ipc.h>
#include <sys/sem.h>
#include <sys/shm.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <assert.h>
#include <limits.h>
#include <ctype.h>
#include <math.h>
#include <time.h>
#include <string.h>
#include <stddef.h>

#include "cmdline.h"
#include "debug.h"
#include "memory.h"
#include "hashtables.h"
#include "listas.h"
#include "palz.h"

int running = 1;

/* Functions */

/**
* Reads the headers of a file to check if it's a PALZ file.
* Creates and returns the dictionary for the file, if it's a valid PALZ file.
* Function re-used from class exercises.
*
* @param filename - the name of the file to be read
* @param *dictionary_size - pointer to the size of the dictionary
* @param *fptr - pointer to the file address
* @return the dictionary after being allocated and read from file
*/

void decompress(const char *filename){
	unsigned int dictionary_size=0;
	char **dictionary = NULL;
	
	FILE *fptr = fopen(filename, "r");
		if(fptr==NULL){
			fprintf(stderr, "\nCould not open %s! Aborting.\n", filename);
		}
		else{
		dictionary = read_header(filename, &dictionary_size, fptr);
		
		if (dictionary != NULL){
			decompress_file(filename, dictionary, &dictionary_size, fptr);
			free_dictionary(dictionary, dictionary_size);
		}
		fclose(fptr);
	}
}

char ** read_header(const char *filename, unsigned int *dictionary_size, FILE *fptr){
	char *ver = strdup(filename);
		if(verify_valid_file(ver)){
			return NULL;		
		}
	FREE(ver);
		if(fptr == NULL){
				WARNING("%s: fopen failed.\n", filename);
				return NULL;
		}

		char line[MAX_LINE];

		if(fgets(line, MAX_LINE, fptr) == NULL){
				WARNING("%s: fgets failed.\n", filename);
				return NULL;
		}
		
		if(is_header_PALZ(line)==0){
				fprintf(stderr, "Failed: %s is not a valid .palz file\n", filename);
				return NULL;
		}

		(*dictionary_size) = 0;
		
		if(fgets(line, MAX_LINE, fptr) == NULL){
			WARNING("%s: fgets failed.\n", filename);
			return NULL;
		}
		
		int valid_size = is_valid_size(line, dictionary_size);
		if(valid_size == 0){
			fprintf(stderr, "Failed: %s is corrupted", filename);
			return NULL;
		}

		if(valid_size == -1){
			fprintf(stderr, "Failed: %s dictionary is too big", filename);
			return NULL;
		}

		if(dictionary_size==NULL){
			fprintf(stderr, "Failed: %s is corrupted\n", filename);
			return NULL;
		}

		if((*dictionary_size)>(exp2(32)-2)){
			fprintf(stderr, "Failed: %s dictionary is too big\n", filename);
			return NULL;
		}

	unsigned int i;
	(*dictionary_size)+=15;

	if(bytes_for_int((*dictionary_size))>3){
		fprintf(stderr, "Failed: %s dictionary is too big\n", filename);
		return NULL;
	}

	char **dictionary = MALLOC((*dictionary_size) * sizeof(char *));
	for(i = 0;i < 15;i++){
		dictionary[i] = MALLOC(2 * sizeof(char));
	}
	dictionary[1]="\n";
	dictionary[2]="\t";
	dictionary[3]="\r";
	dictionary[4]=" ";
	dictionary[5]="?";
	dictionary[6]="!";
	dictionary[7]=".";
	dictionary[8]=";";
	dictionary[9]=",";
	dictionary[10]=":";
	dictionary[11]="+";
	dictionary[12]="-";
	dictionary[13]="*";
	dictionary[14]="/";

	unsigned int verify_size=15;
	for(i = 15; i < (*dictionary_size); i++){
		if (fgets(line, MAX_LINE, fptr) == NULL){
			fprintf(stderr, "Failed: %s is corrupted\n", filename);
			return NULL;
		}
		line[strlen(line) - 1] = '\0';
		dictionary[i] = MALLOC((strlen(line) + 1) * sizeof(char));
		dictionary[i] = strdup(line);
		verify_size++;
	}

	if((*dictionary_size) < verify_size){
		fprintf(stderr, "Failed: %s dictionary is too big", filename);
		return NULL;
	}

	if((*dictionary_size) > verify_size){
		fprintf(stderr, "Failed: %s dictionary is too big", filename);
		return NULL;
	}

	return dictionary;
}


/**
* Frees dictionary memory.
* Function re-used from class exercises.
*
* @param **dictionary - pointer to the dictionary
* @param dictionary_size - size of the dictionary
*/

void free_dictionary(char **dictionary, const unsigned int dictionary_size){
		unsigned int i;
		for(i = 15; i < dictionary_size; i++){
            free(dictionary[i]);
		}
		free(dictionary);
}

/**
* Checks if file header is "PALZ".
* Function re-used from class exercises.
*
* @param header_first_row - the first line read of the provided file
* @return an integer with value 1 if file is PALZ
*/

int is_header_PALZ(const char* header_first_row){
	int check=0;
	// If first row = "PALZ", return check = 1, else, returns check = 0
	if (!strcmp(header_first_row,PALZ)){
		check=1;
	}
	return check;
}

/**
* Checks if dictionary size read is valid integer and inside bounds.
* Function re-used from class exercises.
*
* @param *size_str - line being evaluated
* @param *value - the value being validated
* @return an integer based on the result of the check
* 1 if size if valid
* 0 if invalid
* -1 is larger than unsigned int max value
*/

int is_valid_size(const char *size_str, unsigned int *value){
	char *endptr;
	errno=0;
	long long int n = strtoll(size_str, &endptr, 10);
	
	if(endptr[0] != '\0' && endptr[0] != '\n'){
		return 0;
	}

	if(errno != 0){
		return 0;
	}

	if(n<0){
		return 0;
	}

	if(n>UINT_MAX){
		return -1;
	}
	
	*value = n;
	return 1;
}

/**
* Decompression of a palz file.
* Receives filename and dictionary for the file,
* decompresses it and saves it in a new file.
*
* @param *filename - name of the file being decompressed
* @param *dictionary_size - size of the dictionary of the file
* @param *fptr - pointer to the file opened
*/

void decompress_file(const char *filename, char **dictionary, unsigned int *dictionary_size, FILE *fptr){
	char new_filename[MAX_LINE];
	char option=NULL;
	int cont=1;

	if(strcmp(string_to_upper(filename_has_PALZ(filename)),"PALZ")==0){	//finds .palz in file extension and removes it
		strcpy(new_filename, filename);
		new_filename[strlen(new_filename)-5]='\0';
	}
	else
	{	//case file doesn't have .palz extension
		strcpy(new_filename, filename);
		do
		{		//ask user if he wants to replace original file
			printf("\nWARNING: Didn't find .palz extension in the filename given.\nDo you want to replace the original file? (y/n): ");
			scanf(" %c", &option);
		} while (toupper(option)!='N' && toupper(option)!='Y');

		if(toupper(option)=='N'){
			printf("Operation cancelled by user. %s won't be decompressed.\n", filename);
			cont=0;		//won't allow decompression to go on
		}
	}

	if(cont!=0){

	unsigned int i=0;
	unsigned int code=0;
	unsigned int old_code=0;
	unsigned int bytes=0;

	bytes = bytes_for_int((*dictionary_size));

		FILE *fptr_new = fopen(new_filename, "w");
		if(fptr_new==NULL){
			fprintf(stderr, "\nERROR: Could not open %s!\n", new_filename); 
		}
		else{

		while(((fread(&code, bytes, 1, fptr))==1) && cont==1){				// Repeats the loop while fread returns 1 and cont==1;
			if(code>(*dictionary_size)-1){									// Verify if code exists in dictionary (-1 because dictionary was
					fprintf(stderr, "Failed: %s is corrupted\n", filename);	//started at 15)
					cont=0; 												// Interrupts the cycle if the code doesn't exist in the dictionary
			}
			else{
				if(code!=0){
					fprintf(fptr_new, "%s", dictionary[code]);
					old_code=code;
				}

				else{												// Case code==0, reads next number (number of repeats) and prints
					fread(&code, bytes, 1, fptr);					// the corresponding word using the variable 'old_code'.
					for(i=0;i<code;i++){
						fprintf(fptr_new, "%s", dictionary[old_code]);
					}
				}
			}
		}
		
		// Calculate decompression ratio
		if(cont==1){
			print_file_compression(new_filename, fptr_new, fptr);
		}

		fclose(fptr_new);
		}
	}
}

/**
* Verify if file has .PALZ extension.
*
* @param *filename - name of the file to check for extension
* @return string 'PALZ'
*/

char *filename_has_PALZ(const char *filename) {
    char *extension = strrchr(filename, '.');
    if(!extension || extension == filename){
		return "";
	}
    return extension+1;
}

/**
* Receives a string and returns it in uppercase.
*
* @param string to be set to uppercase
* @return received string in uppercase
*/

char *string_to_upper(char *str){
    char *newstr;

    newstr = strdup(str);
    unsigned int i=0;
    for(i=0;i<=strlen(newstr);i++){
		newstr[i]=toupper(newstr[i]);
	}
    return newstr;
}

/**
* Receives a value and checks the number of bytes required to store it.
*
* @param value - number of bytes to be checked
* @return number of bytes necessary to store the int value sent to it
*/

unsigned int bytes_for_int(unsigned int value) {
	if (value <= 256){
		return 1;
	}
	if (value <= 65536){
		return 2;
	}
	if (value <= 16777216){
		return 3;
	}
	return 4;
}

/**
* Creates the signal handler.
*/

void install_signal_handler(void) {

    struct sigaction act;

    // Registers signal handling function
    act.sa_handler = trata_sinal;

    // Default behavior: reliable
    act.sa_flags = 0;

    // Mask without signals so that we don't block them
    sigemptyset(&act.sa_mask);


   // SIGUSR1
   if (sigaction(SIGUSR1, &act, NULL) < 0){
      ERROR(1, "sigaction - SIGUSR1: failed to install signal handler");
   }

   // SIGINT
   if (sigaction(SIGINT, &act, NULL) < 0){
      ERROR(2, "sigaction - SIGINT: failed to install signal handler");
   }

	int continua=0;

	while (continua){
		// ciclo principal
		pause(); // Espera bloqueante
		printf("Pause interrompido\n");
	}
}

/**
* Treats signals when they are sent to the program while running.
*
* @param signal - the int value of the signal sent to the program
*/

void trata_sinal(int signal){
	int aux;

	// Copia da variavel global errno
	aux = errno;

  	if (signal == SIGUSR1){
		printf("Received the signal SIGUSR1 (%d)\n", signal);
	}

	if (signal == SIGINT){
		time_t current_time = time(NULL);
		struct tm *local_time = localtime(&current_time);
		printf("Stopped by the user @ %s", asctime(local_time));

	}
	errno = aux;
}

/**
* Receives a signal and sets running to 0.
*
* @param signal - value of the received signal
*/

void handler(int signal){
	(void)signal;
	running = 0;
}

/**
* Receives a the name of a file and begins the compression process.
* Creates and organizes lists and arrays to end up with an organized
* and indexed hashtable to be used in the file compression.
*
* @param *filename - pointer to filename of the file being compressed
*/

void compress(char *filename){
	FILE *fptr = fopen(filename, "r");
	int bytes;
	if(fptr==NULL){
		fprintf(stderr, "Could not open %s to compress\n", filename);
		exit(0);
	}
	
	if(verify_valid_file(filename) == 0){
		return;
	}
	
	HASHTABLE_T *hashtable = find_words(fptr, DELIMITERS);
	if (hashtable != NULL){
		LISTA_GENERICA_T *list = tabela_criar_lista_chaves(hashtable);
		int num_list_elements = tabela_numero_elementos(hashtable);
		int i;
		int * idx_ptr;
		
		bytes = bytes_for_int((num_list_elements+15));
		if(bytes>3){
			fprintf(stderr, "Failed: %s dictionary is too big\n", filename);
			exit(1);
		}
		
		char **dictionary_array = MALLOC(num_list_elements * sizeof(char *));
		if(dictionary_array == NULL){
			fprintf(stderr, "Error while allocating memory!\n");
		}
		else{
			list_to_array(list, dictionary_array);
			lista_destruir(&list);
			qsort(&dictionary_array[0], num_list_elements, sizeof(char *), cmpstring);
			
			for(i=0;i<num_list_elements;i++){
				idx_ptr = tabela_consultar(hashtable, dictionary_array[i]);
				if (idx_ptr == NULL){
					DEBUG("Failed to find %s in hashtable (i=%d)\n", dictionary_array[i], i);
					exit(1);
				}
				else{
					*idx_ptr = i+15;	
				}
			}
			
			char new_filename[MAX_LINE];
			strcpy(new_filename, filename);
			strcat(new_filename, ".palz");
			
			FILE *fptr_new = fopen(new_filename, "w");
			if(fptr_new==NULL){
				fprintf(stderr, "ERROR: Could not open %s!\n", new_filename);
			}
			
			else{	
				fprintf(fptr_new, "%s", PALZ);							//print PALZ in file
				fprintf(fptr_new, "%d\n", num_list_elements);			//print number of dictionary elements
				for(i=0; i<num_list_elements; i++){
					fprintf(fptr_new, "%s\n", dictionary_array[i]);		//print dictionary
				}
				
				if((fseek(fptr, 0, SEEK_SET))!=0){
					fprintf(stderr, "ERROR: Could not return to the start of %s!\n", filename);
				}
				else{
					compress_file(fptr, fptr_new, *hashtable, num_list_elements);
				}
				print_file_compression(filename, fptr, fptr_new);
				fclose(fptr_new);
			}
			fclose(fptr);
			
			free(dictionary_array);
		}
		tabela_destruir(&hashtable);
	}
}

/**
* Receives a file pointer and the delimiters to create an hashtable
* for future use.
*
* @param *fptr - pointer to the file
* @param *delimiters - pointer to the string of delimiters
* @return unorganized hashtable
*/

HASHTABLE_T *find_words(FILE *fptr, char *delimiters){
	unsigned int n = 1024;
	char *line = MALLOC(n);
	int *v = NULL;
	
	HASHTABLE_T *hashtable = tabela_criar(65535, free);
	while(getline(&line, &n, fptr) > 0){
		char *last = NULL;
		char *word = strtok_r(line, delimiters, &last);
		
		while(word){
			if(tabela_consultar(hashtable, word) == NULL){
				v = MALLOC(sizeof(int));
				if(v == NULL){
					fprintf(stderr, "Error while allocating memory!\n");
					return NULL;
				}
				*v = 0;
				tabela_inserir(hashtable, word, v);
			}
			word = strtok_r(NULL, delimiters, &last);
		}
	}
	
	if(!feof(fptr)){
		ERROR(ERR_FILE, "Error while reading file.\n");
	}
	
	free(line);
	
	return hashtable;
}

/**
* Receives a list and an array and copies the list content to the array.
* This array will later be used in a qsort to organize the hashtable
* afterwards.
*
* @param *list - pointer to the list
* @param **dictionary_array - pointer to the array
*/

void list_to_array(LISTA_GENERICA_T *list, char **dictionary_array){
	ITERADOR_T *iterator = lista_criar_iterador(list);
	int i = 0;
	char* word;
	
	while((word = iterador_proximo_elemento(iterator))){
		dictionary_array[i] = word;
		i++;
	}
	iterador_destruir(&iterator);
}

/**
* This function is used merely for comparison purposes on qsort
*/

int cmpstring(const void *p1 , const void *p2){
	return strcmp(* (char * const *) p1, * (char * const *) p2);
} 

/**
* This function is used to print the codes into the new compressed file.
* It contains various checks to prevent errors, and iterates the
* uncompressed file to lookup its words in the hashtable and print on
* the new compressed file.
*
* @param *fptr - pointer to the file
* @param *fptr_new - pointer to the new file 
* @param hashtable - the organized hashtable containing the dictionary
* @param num_list_elements - number of elements in the hashtable
*/

void compress_file(FILE *fptr, FILE *fptr_new, HASHTABLE_T hashtable, 
int num_list_elements){
	int code=0, i, foundDelim=0, wordFound=0, delimPosition, lastDelim=0, 
	useLastDelim=0, countDelims=1;
	char c1, delim[14], c2, word[1000];
	
	int bytes = bytes_for_int(num_list_elements+15);
	strcpy(delim, DELIMITERS);
	
	while(((c1 = fgetc(fptr)) != EOF)) {
		useLastDelim=0;
		foundDelim=0;
		for(i=0;i<14;i++){
			if(c1 == delim[i]){
				delimPosition=i+1;
				
				if(delimPosition == lastDelim){
					useLastDelim=1;
				}
				foundDelim=1;
				break;
			}
		}
		
		if(useLastDelim==1){
			foundDelim=0;
			fwrite(&foundDelim, bytes, 1, fptr_new); //writes a 0
			while(delim[lastDelim-1] == (c2 = fgetc(fptr))){
				countDelims++;
			}
			//get byte limit value
			int limitValue = get_limit_value(bytes);
		
			while(countDelims>limitValue){
				fwrite(&limitValue, bytes, 1, fptr_new);
				fwrite(&foundDelim, bytes, 1, fptr_new);
				countDelims-=limitValue;
			}
			fwrite(&countDelims, bytes, 1, fptr_new);
			countDelims=1;
			//verify if last character read is delim
			for(i=0;i<14;i++){
				if(c2 == delim[i]){
					foundDelim=2;
				}
			}
		}
		
		if(foundDelim==2){
			for(i=0;i<14;i++){
				if(delim[i]==c2){
					delimPosition=i+1;
				}
			}
			fwrite(&delimPosition, bytes, 1, fptr_new);
			lastDelim = delimPosition;
		}
		
		if(foundDelim==0){
			build_word(&lastDelim, word, useLastDelim, &c1, c2, &wordFound);
		}
		
		if(foundDelim==1){
			if(wordFound==1){ // if found word before
				code = get_word_value(hashtable, word);
				fwrite(&code, bytes, 1, fptr_new);
				memset(word,0,strlen(word));
			}
			
			fwrite(&delimPosition, bytes, 1, fptr_new);
			
			if(delimPosition!=0){
				lastDelim = delimPosition;
			}
			wordFound=0; //reset to 0
		}
	}
	if(strlen(word)>1){ // verify if word was found after last delim
		code = get_word_value(hashtable, word);
		fwrite(&code, bytes, 1, fptr_new);
	}
}

/**
* Receives a hashtable and a string and returns its index value in the
* table.
*
* @param hashtable - the organized hashtable to be looked up
* @param *word - the word being looked up in the hashtable
* @return *v - the index of the word requested
*/

int get_word_value(HASHTABLE_T hashtable, char *word){
	int *v=NULL;
	
	v = tabela_consultar(&hashtable, word);
	if(v == NULL){
		DEBUG("Could not find word '%s' in dictionary\n", word);
	}
	
	return *v;
}

/**
* Returns the maximum value of repetitions depending on the number of
* bytes per block
*
* @param bytes - number of bytes per block in this file
* @return limitValue - maximum number of repetitions
*/

int get_limit_value(int bytes){
	int limitValue;
	if(bytes==1){
		limitValue=255;
	}
	else{
		if(bytes==2){
			limitValue=65535;
		}
		else{
			limitValue=16777215;
		}
	}
	return limitValue;
}

/**
* Builds the word to be looked up in the hashtable. This function is
* called until a delimiter is found, after which, the word is looked up
* on the hashtable, printed to the file and then reset, to start
* building a new word.
*
* @param *lastDelim - pointer to the last delimiter used
* @param *word - pointer to the word string being built
* @param useLastDelim - check if needs to use last delimiter (Repetitions)
*/

void build_word(int *lastDelim, char *word, int useLastDelim, 
 char c1[1000], char c2, int *wordFound){
	*lastDelim=0;
	size_t len = strlen(word);
	char *word2 = malloc(len + 1 + 1);
	strcpy(word2, word);
	
	if(useLastDelim==0){
		word2[len] = *c1;
	}
	else{
		word2[len] = c2;
	}
	
	word2[len + 1] = '\0';
	
	strcpy(word, word2);
	free(word2);
	*wordFound=1;
}

/**
* Receives a filename and verifies if the extension is .palz
*
* @param *filename - pointer to the file's name
* @return an integer with value 0 if finds a .palz extension or 1 if not
*/

int verify_valid_file(char *filename){
	if(strlen(filename)>5){
		if(strcmp(string_to_upper(filename_has_PALZ(filename)),"PALZ")==0){
			return 0;
		}
	}
	return 1;
}

/**
* Prints the compression ratio of a file
*
* @param *filename - pointer to the file's name
* @param *decompressed - pointer to the decompressed file
* @param *compressed - pointer to the compressed file
*/

void print_file_compression(char *filename, FILE *decompressed, FILE *compressed){
	float file_de_size = ftell(decompressed);
	float file_com_size = ftell(compressed);
	float comp = ((1-(file_com_size/file_de_size))*100);
	
	if(comp < 0){
		comp = 0;
	}
	
	printf("Compression ratio: %s %.2f%%\n", filename, comp);
}
