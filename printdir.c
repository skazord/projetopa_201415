/**
* @file printdir.c
* @brief This file has the functions used to handle folders and folder decompression.
* @author Beatriz Franco, Eduardo Andrade
*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <dirent.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <unistd.h>

#include "debug.h"
#include "printdir.h"
#include "memory.h"
#include "palz.h"

/**
* Receives a folder directory and its depth and looks for all the palz files in its subdirectories
*
* @param dirname - the name of the directory to be searched
* @param depth - the depth of the folder in the disk
* @param mode - 1 = decompress ; 2 = compress
*/

void show_dir(const char *dirname, const unsigned int depth, int mode) {
        DIR *dirptr;
        struct dirent entry, *result = NULL;
        struct stat stats;
        char *fullname = NULL;

        if ((dirptr = opendir(dirname)) == NULL){
                ERROR(2, "Can't open directory %s", dirname);
        }

        while (readdir_r(dirptr, &entry, &result) == 0 && result != NULL) {

 			//ignore "." and ".."
			if(strcmp(entry.d_name,".")==0 || strcmp(entry.d_name,"..")==0){
				continue;
			}

			build_fullname(dirname, entry.d_name, &fullname);

			if(lstat(fullname, &stats)!=0){
				ERROR(ERR_FILE, "lstat failed on folder %s\n", fullname);
			}

			if(S_ISDIR(stats.st_mode)){
				if(depth!=0){
					if(mode==1){
						show_dir(fullname, depth-1, 1);
					}
					else{
						show_dir(fullname, depth-1, 2);
					}
				}
			}
			else
			{
				if(mode==1){
					decompress(fullname);
				}
				else{
					compress(fullname);
				}
			}
		}

                if (lstat(fullname, &stats) != 0) {
                       ERROR(ERR_FILE, "lstat() failed");
                }

        if (result != NULL) {
               ERROR(ERR_FILE, "readdir_r() failed");
        }

        //Free what must be freed
        free(fullname);

        closedir(dirptr);
}

/**
* Creates the full name of the directory for a palz file
*
* @param parent - the directory path
* @param entry - the name of the file
* @param fullname - pointer to the full directory path, that is used outside of this function
*/

void build_fullname(const char * parent, const char * entry, char ** fullname){
	
	if((*fullname = realloc(*fullname, (strlen(parent) + strlen(entry) + 2) * sizeof(char))) == NULL){
		//2 para o '/'+'\0' -> aaa/bbb\0
		ERROR(ERR_FILE, "Could not realloc %s\n", fullname);
	}

	if(parent[strlen(parent)-1]=='/'){
		sprintf(*fullname, "%s%s", parent, entry);
	}
	else
	{
		sprintf(*fullname, "%s/%s", parent, entry);
	}
}


