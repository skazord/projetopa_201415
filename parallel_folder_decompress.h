#ifndef PARALLEL_FOLDER_DECOMPRESS_H
#define PARALLEL_FOLDER_DECOMPRESS_H

typedef struct 
{
	char **buffer;
	int buffer_size;
	int index_leitura;
	int index_escrita;
	int total;
	pthread_mutex_t mutex;
	pthread_cond_t cond;
	int stop;
} PARAM_T;

/*Declaração de funções */

void parallel_folder_decompress(char *dir, int max_threads);
void parallel_folder_compress(char *dir, int max_threads);
void producer_compress(char *dir, PARAM_T *params);
void *consumer_compress(void *arg);
void producer_decompress(char *dir, PARAM_T *params);
void *consumer_decompress(void *arg);

#endif
