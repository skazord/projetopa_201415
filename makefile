# Makefile for Advanced Programming
#
# authors Beatriz Franco & Eduardo Andrade

# Libraries to include (if any)
LIBS=-pthread 

# Compiler flags
CFLAGS=-Wall -W -Wmissing-prototypes

# Indentation flags
IFLAGS=-br -brs -npsl -ce -cli4

# Name of the executable
PROGRAM=palz

# Prefix for the gengetopt file (if gengetopt is used)
PROGRAM_OPT=#palz_opt

# Object files required to build the executable
PROGRAM_OBJS=main.o cmdline.o memory.o palz.o debug.o printdir.o hashtables.o listas.o parallel_folder_decompress.o parallel_folder_compress.o # ${PROGRAM_OPT}.o

# Clean and all are not files
.PHONY: clean all docs indent debugon

all: ${PROGRAM}

# compilar com depuracao
debugon: CFLAGS += -D SHOW_DEBUG -g
debugon: ${PROGRAM}

${PROGRAM}: ${PROGRAM_OBJS}
	${CC} -o $@ ${PROGRAM_OBJS} ${LIBS}

# Dependencies
main.o: main.c cmdline.h memory.h palz.h debug.h printdir.h parallel_folder_decompress.h #${PROGRAM_OPT}.h
${PROGRAM_OPT}.o: ${PROGRAM_OPT}.c ${PROGRAM_OPT}.h

cmdline.o: cmdline.c cmdline.h
memory.o: memory.c memory.h
palz.o: palz.c palz.h memory.h debug.h hashtables.h listas.h parallel_folder_decompress.h cmdline.h
debug.o: debug.c debug.h
printdir.o: printdir.c printdir.h
hashtables.o: hashtables.c hashtables.h
listas.o: listas.c listas.h
parallel_folder_decompress.o: parallel_folder_decompress.c parallel_folder_decompress.h
parallel_folder_compress.o: parallel_folder_compress.c parallel_folder_decompress.h

#how to create an object file (.o) from C file (.c)
.c.o:
	${CC} ${CFLAGS} -c $<

# Generates command line arguments code from gengetopt configuration file
${PROGRAM_OPT}.h: ${PROGRAM_OPT}.ggo
	gengetopt < ${PROGRAM_OPT}.ggo --file-name=${PROGRAM_OPT}

clean:
	rm -f *.o core.* *~ ${PROGRAM} *.bak ${PROGRAM_OPT}.h ${PROGRAM_OPT}.c

docs: Doxyfile
	doxygen Doxyfile

Doxyfile:
	doxygen -g Doxyfile

indent:
	indent ${IFLAGS} *.c *.h
