/**
* @file printdir.h
* @brief This file has the declarations of the functions in printdir.c.
* @author Beatriz Franco, Eduardo Andrade
*/

#ifndef PRINTDIR_H
#define PRINTDIR_H

/*Declaração de funções */

void build_fullname(const char * parent, const char * entry, char ** fullname);
void show_dir(const char *dirname, const unsigned int depth, int mode);

#endif
