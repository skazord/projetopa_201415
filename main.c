#include <stdio.h>
#include <stdlib.h>
#include <limits.h>
#include <string.h>
#include <time.h>
#include <sys/time.h>
#include <signal.h>

#include "parallel_folder_decompress.h"
#include "cmdline.h"
#include "memory.h"
#include "palz.h"
#include "printdir.h"


#define ERR_ARGS 1
#define DEPTH 15
#define MAX_THREADS 10

int main(int argc, char *argv[]) {
	
	(void)argc;
	(void)argv;
	
	clock_t t1, t2;
	t1 = clock();

	struct gengetopt_args_info ArgsInfo;
	if (cmdline_parser(argc, argv, &ArgsInfo) !=0){
		fprintf(stderr,"Erro: execução de cmdline_parser\n");
		exit(1);
	}
	
	install_signal_handler();
	
	if (ArgsInfo.about_given){
		printf("About PALZ\n\n");
		printf("Authors:\n");
		printf("\tBeatriz Franco 2120181\n");
		printf("\tEduardo Andrade 2131105\n");
		printf("\nAdvanced Programming Project 2014/15 - Computer Engineering ESTG Leiria\n\n");		
	}

	if (ArgsInfo.decompress_given){
		decompress(ArgsInfo.decompress_arg);
	}

	if (ArgsInfo.folder_decompress_given){
		show_dir(ArgsInfo.folder_decompress_arg, DEPTH, 1);
	}
	
	if (ArgsInfo.compress_given){
		compress(ArgsInfo.compress_arg);
	}
	
	if (ArgsInfo.parallel_folder_compress_given && ArgsInfo.compress_max_threads_arg <= MAX_THREADS){
		parallel_folder_compress(ArgsInfo.parallel_folder_compress_arg, ArgsInfo.compress_max_threads_arg);
	}
	
	if (ArgsInfo.parallel_folder_decompress_given && ArgsInfo.decompress_max_threads_arg <= MAX_THREADS){
		parallel_folder_decompress(ArgsInfo.parallel_folder_decompress_arg, ArgsInfo.decompress_max_threads_arg);
	}
	
	t2 = clock();   

	float diff = (((float)t2 - (float)t1) / 1000000.0F ) * 10;   
	printf("Execution time: %.2fs\n",diff); 
 
	return 0;
}
